# Pôtichiens

Site d'adoption de chiens, parce qu'on aime les chiens !

## Installation du projet

- [ ] Cloner le projet depuis [https://gitlab.com/maxime.garcia.dev/symfony-studies-projects.git](https://gitlab.com/maxime.garcia.dev/symfony-studies-projects.git)
- [ ] Aller dans le dossier et lancer `composer install`
