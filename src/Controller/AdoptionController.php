<?php

namespace App\Controller;

use App\Entity\Adoption;
use App\Repository\AdoptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/annonces")
 */
class AdoptionController extends AbstractController
{
    /**
     * @Route("/", name="adoption_index")
     */
    public function announces(AdoptionRepository $adoptionRepository): Response
    {
        $adoptions = $adoptionRepository->findAll();

        return $this->render('adoption/adoption.html.twig',
            ['adoptions' => $adoptions]);
    }

    /**
     * @Route("/{id}", name="adoption_show")
     */
    public function show(Adoption $adoption): response{

        return $this->render('adoption/show.html.twig',[
            'adoption' => $adoption,
        ]);
    }
}
