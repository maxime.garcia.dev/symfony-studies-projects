<?php

namespace App\DataFixtures;

use App\Entity\Adoption;
use App\Repository\DogRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AdoptionFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var DogRepository
     */
    private $dogRepository;

    public function __construct(DogRepository $dogRepository)
    {
        $this->dogRepository = $dogRepository;
    }

    public function load(ObjectManager $manager)
    {
        $dogs = $this->dogRepository->findAll();

        foreach ($dogs as $dog){

            $adoption = new Adoption();
            $adoption->setName('Un toutou beaucoup trop gentil!');
            $adoption->setDescription('Adoptez le vite!');
            $adoption->addDog($dog);
            $manager->persist($dog);
            $manager->persist($adoption);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [DogFixtures::class];
    }
}
