<?php


namespace App\DataFixtures;


use App\Entity\Breed;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
class BreedFixtures extends Fixture
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $malamute = new Breed();
        $malamute->setName('Malamute');
        $malamute->setDescription("Le Malamute de l’Alaska possède une santé de fer. Il supporte les conditions hivernales à merveille mais a plus de mal avec les grosses chaleurs. Son entretien est minime et l’éduquer se révèle assez aisé. De plus, ce canidé est un adorable compagnon de vie. Il plaira à toutes les familles car il n’est jamais agressif et adore la compagnie des enfants. Le Malamute de l'Alaska est typiquement un chien de traîneau arctique. Il est taillé pour l'activité, la force et l'endurance : ossature forte, musculature bien développée, bien plus que celle du Husky Sibérien avec lequel il est souvent confondu. Son port de tête haut et fier accroît son élégance naturelle.");
        $manager->persist($malamute);

        $akitaInu = new Breed();
        $akitaInu->setName('Akita Inu');
        $akitaInu->setDescription("L'Akita Inu est un chien majestueux, de constitution robuste et au caractère bien trempé. De type Spitz, il est de grande taille et bien proportionné. Ce chien séduit par son aspect peluche et la grande sérénité qu'il dégage. L’Akita Inu est un chien parfait en tout point pour la vie de famille. Il peut être à la fois sportif ou non, tout dépend du besoin de son maître. Il est calme, prévenant, attentionné, indépendant, fidèle et n’aboie que très modérément. Son éducation est plutôt aisée et sa santé, malgré 2 maladies héréditaires, assez solide.");
        $manager->persist($akitaInu);

        $pekinois = new Breed();
        $pekinois->setName('Pékinois');
        $pekinois->setDescription("Le Pékinois, ou Epagneul Pékinois, est un chien de petite taille, court sur pattes, à la silhouette modérément trapue et à l'aspect léonin (évoquant le lion). Il dégage une expression d'intelligence et de vivacité. Dignité et noblesse caractérisent aussi son allure. Le Pékinois n’est pas un chien destiné à toutes les familles. Il n’est pas forcément très à l’aise en compagnie des enfants. Son peu de besoin de dépense énergétique convient parfaitement aux personnes âgées. Il est doux et calme avec son maître, plus vif et méfiant avec les étrangers. Son entretien n’est pas bien compliqué même s’il faut veiller à plusieurs aspects pour une vie en bonne santé.");
        $manager->persist($pekinois);

        $caniche = new Breed();
        $caniche->setName('Caniche');
        $caniche->setDescription("Le Caniche est l’un des chiens les plus populaires en France. Les familles adorent sa bouille joviale et très mignonne, sa douceur, sa quiétude et son envie incessante de bouger. Il apporte du dynamisme dans les vies de chacun. Il est en plus très sociable, ne demande pas un entretien de tous les instants, et a la particularité d’avoir une santé robuste.");
        $manager->persist($caniche);

        $manager->flush();
    }
}