<?php


namespace App\DataFixtures;


use App\Entity\Image;
use App\Repository\DogRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
class ImageFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @var DogRepository
     */
    private $dogRepository;

    public function __construct(DogRepository $dogRepository)
    {
        $this->dogRepository = $dogRepository;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $dogs = $this->dogRepository->findAll();

        foreach ($dogs as $dog) {
            $image = new Image();
            $image->setPath('https://www.woopets.fr/assets/races/000/010/big-portrait/akita-inu.jpg');
            $image->setDog($dog);

            $manager->persist($image);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [DogFixtures::class];
    }
}