<?php


namespace App\DataFixtures;


use App\Entity\Dog;
use App\Repository\BreedRepository;
use DateTime;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
class DogFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @var BreedRepository
     */
    private $breedRepository;

    public function __construct(BreedRepository $breedRepository)
    {
        $this->breedRepository = $breedRepository;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $malamute = $this->breedRepository->findOneBy(['name' => 'Malamute']);
        $akitaInu = $this->breedRepository->findOneBy(['name' => 'Akita Inu']);
        $caniche = $this->breedRepository->findOneBy(['name' => 'Caniche']);
        $pekinois = $this->breedRepository->findOneBy(['name' => 'Pékinois']);

        $date = new DateTime('2020-01-31');

        $hulk = new Dog();
        $hulk->setName('Hulk');
        $hulk->setDescription('Le plus gentil des toutous!');
        $hulk->addBreed($malamute);
        $hulk->addBreed($akitaInu);
        $hulk->setBirthDate($date);

        $noisette = new Dog();
        $noisette->setName('Noisette');
        $noisette->setDescription('Une crème parmi les cèmes');
        $noisette->addBreed($caniche);
        $noisette->addBreed($pekinois);
        $noisette->setBirthDate($date);


        $manager->persist($hulk);
        $manager->persist($noisette);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [BreedFixtures::class];
    }
}